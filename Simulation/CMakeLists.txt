    if(COMMAND cmake_policy)
      cmake_policy(SET CMP0003 NEW)
    endif(COMMAND cmake_policy)

SET (SIMULATION_LINK_LIBRARIES freeglut GL GLU ${PROJECT_PATH}/AntTweakBar/libAntTweakBar.a)
SET (SIMULATION_DEPENDENCIES freeglut)

# PROJECT(Simulation)
ADD_EXECUTABLE(Simulation
	  main.cpp
	  Particle.cpp
	  Particle.h  
	  SimMath.cpp
	  SimMath.h
	  TimeManager.cpp
	  TimeManager.h
	  ${VIS_FILES}                    
	  ${PROJECT_PATH}/Common/Config.h			  
	  ${PROJECT_PATH}/Common/StringTools.h			  
	  ${PROJECT_PATH}/Common/StringTools.cpp
	  ${PROJECT_PATH}/Common/timing.h			  	  
	  ${PROJECT_PATH}/Common/timing.cpp
	  CMakeLists.txt
)

find_package( Eigen3 REQUIRED )
include_directories( ${EIGEN3_INCLUDE_DIR} )

ADD_DEPENDENCIES(Simulation ${SIMULATION_DEPENDENCIES})
TARGET_LINK_LIBRARIES(Simulation ${SIMULATION_LINK_LIBRARIES})
VIS_SOURCE_GROUPS()
# COPY_ANTTWEAKBAR_DLL(Simulation)
